var redis = require('redis'),
    os = require('os'),
    assert = require('assert')

var e = {
    client: null
};

function addToRedis(_path, _data) {
    try {
        e.client.ttl("set_" + _path, function(_error, _r){
            if (_error) {
                console.log(_error);
                process.exit(0);
            }
            setMagicKey(_path).then(() => {
                e.client.sadd("set_" + _path, _data);
                e.client.pexpire("set_" + _path, 2000)
            }, () => {});
            setTimeout(function () {
                addToRedis(_path, _data)
            }, 500);
        });
    } catch (_error) {
        console.log(_error);
    }
}

function setMagicKey(_key) {
    return new Promise((res, rej) => {
        e.client.hget("magicwords", _key, (_e, _d) => {
            if (_e) rej(_e)
            if (!_d) {
                e.client.hset("magicwords", _key, new Date().toISOString(), (_e, _d) => {
                    if (_e) rej(_e)
                    res()
                })
            } else res()
        })
    });
}

e.init = (_redisClient) => e.client = _redisClient;

e.register = (_path, _data) => {
    return new Promise((resolve, reject) => {
        if (!_path) reject('Missing config path.')
        if (!_data) reject('Missing config value')
        let ipAddress = process.env.PUTTU_IP ? process.env.PUTTU_IP : "localhost";
        let data = _data.protocol.toLowerCase() + '://' + ipAddress + ':' + _data.port + _data.api
        addToRedis(_path, data)
        resolve();
    })
}

e.get = (_path) => {
    var redisSet = "set_" + _path
    return new Promise((resolve, reject) => {
        e.client.srandmember(redisSet, (_e, _d) => {
            if (_e || !_d) {
                reject(_e)
            }
            resolve(_d)
        })
    })
}

e.set = (_key, _data) => {
    return new Promise((resolve, reject) => {
        e.client.set(_key, _data, (_e, _d) => {
            if (_e || !_d) {
                reject(_e)
            }
            resolve(_d)
        })
    })
}

e.getValue = (_key) => {
    return new Promise((resolve, reject) => {
        e.client.get(_key, (_e, _d) => {
            if (_e || !_d) {
                reject(_e)
            }
            resolve(_d)
        })
    })
}

e.getMagicKey = (_key) => {
    return new Promise((res, resj) => {
        e.client.hget("magicwords", _key, (_e, _d) => {
            if (_e) rej(_e)
            res(_d)
        })
    });
}



e.addCache = (key, time, response) => {
    return new Promise((resolve, reject) => {
        if (process.env.PROD_ENV == "true" || process.env.STAGING_ENV == "true") {
            response = JSON.stringify(response);

            client.hset(key, "response", response, (_e, _d) => {
                if (_e || !_d) {
                    reject(_e);
                    return;
                }
                client.expire(key, time, (_e1, _d1) => {
                    if (_e1 || !_d1) {
                        reject(_e1);
                        return;
                    }
                    resolve(_d);
                })
            })

        } else {
            resolve(0);
        }
    });
}


e.getCache = (key) => {
    return new Promise((resolve, reject) => {
        client.hget(key, "response", (_e, _d) => {
            if (_e || !_d) {
                resolve(null);
                return;
            }
            resolve(JSON.parse(_d));
        })
    });
}


e.expireCache = (key) => {
    return new Promise((resolve, reject) => {
        client.expire(key, 0, (_e, _d) => {
            if (_e || !_d) {
                reject(_e);
                return;
            }
            resolve(_d);
        })
    });
}
module.exports = e;